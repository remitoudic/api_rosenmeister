from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from payload import views

urlpatterns = [
    path('payload', views.PayloadList.as_view()),
    path('payload/employee/<int:pk>', views.PayloadDetail.as_view()),
    path('payload/letter_digit/<str:characters>', views.letter_digit.as_view()),
    path('payload/filter/<str:from_>:<str:_to>', views.Payload_filter.as_view()),
    path('payload/average_age_people', views.AverageAge.as_view()),
]

