from django.db import models
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles
from API_rosenmeister import settings

LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted([(item, item) for item in get_all_styles()])


class Payload(models.Model):
    first_name = models.CharField(choices=LANGUAGE_CHOICES, default='XXX', max_length=100)
    last_name = models.CharField(choices=LANGUAGE_CHOICES, default='XXX', max_length=100)
    email = models.EmailField(max_length=254,  unique=True)     
    birthday = models.DateField()
   
    class Meta:
        ordering = ['birthday']
    
    def get_payload_person_email(self):
        return  self.email + " is the email of: " + self.first_name

    def __repr__(self):
        return self.first_name + ' is added.'

      