from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status
from payload.serializers import PayloadSerializer
from payload.models import Payload
from payload import views
from rest_framework.test import APIRequestFactory


client = Client()

class PayloadTest(TestCase):
    """ Test module for payload app """
    client = Client()

    def setUp(self):
        
        Payload.objects.create(first_name = "remi", 
                last_name= "XXXX" , 
                email =  "remitoudic999@gmail.com", 
                birthday= "1989-11-12" )

        Payload.objects.create(first_name = "Paul", 
                last_name= "YYYYY" , 
                email =  "paul999@gmail.com", 
                birthday= "1987-10-12" )
        
    def test_payload_model(self):
        payload_remi = Payload.objects.get( email = "remitoudic999@gmail.com")
        self.assertEqual(payload_remi.get_payload_person_email(), 
                            "remitoudic999@gmail.com is the email of: remi")
    
    def test_average(self):
        # average in years
        response = self.client.get('/payload/average_age_people')
        self.assertEqual(response.data["average"] , 31)

    def test_payload_list(self):
        # two people in the db
        response = self.client.get('/payload')
        self.assertEqual(len(response.data) , 2)

    def test_filter(self):
        # on person filtered out , the one born in 1987 
        response = self.client.get('/payload/filter/1988-12-01:2002-12-01')
        self.assertEqual(len(response.data) , 1) 
    
    def test_letter_digit(self):
        response=self.client.get('/payload/letter_digit/a2b')
        self.assertEqual(set(response.data["result"]) , 
                         set( [ "a2b","a2B","A2b","A2B"]))

  
    
    

 

