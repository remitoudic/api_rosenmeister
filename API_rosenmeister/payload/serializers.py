from rest_framework import serializers
from payload.models import Payload, LANGUAGE_CHOICES, STYLE_CHOICES

class PayloadSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    first_name = serializers.CharField(required=False, allow_blank=True, max_length=100)
    last_name = serializers.CharField(required=False, allow_blank=True, max_length=100)
    email = serializers.CharField(required=False, allow_blank=True, max_length=100)
    birthday = serializers.DateField(format="%d.%m.%Y", required=False, read_only=True)
       
    def create(self, validated_data):
        """
        Create and return a new `payload` instance, given the validated data.
        """
        return Payload.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `payload` instance, given the validated data.
        """
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.email = validated_data.get('email', instance.email)
        instance.birthday = validated_data.get('birthday',instance.birthday)
        instance.save()

        return instance