from payload.models import Payload
from payload.serializers import PayloadSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from itertools import product
from datetime import date
from django_filters import rest_framework as filters


class PayloadList(APIView):
    """
    List all people , or create a new payload.
    """
    def get(self, request, format=None):
        payload = Payload.objects.all()
        serializer = PayloadSerializer(payload, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = PayloadSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class letter_digit(APIView):

    def get(self,request,characters):

        """  a2B => [a2b, a2B, A2b, A2B]  """
    
        # get the upper und lower elements
        seT= [ (Letter_Digit.lower(),
                Letter_Digit.upper()) for Letter_Digit in characters ] 
        
        # clean number duplicates
        clean_sets = map(set,seT)  
        clean_sets2=list( map(list,clean_sets ))
        
        # cartesian product 
        prod= list(product(*clean_sets2)) 
        result = list(map("".join,prod )) 
        
        return Response({"result": result})



class AverageAge(APIView):
    """
    average of ages of the people in  db 
    """
    def get(self, request, format=None):
        payload = Payload.objects.all()
        sum_age = sum([self.find_age(perso) for perso in payload ])
        average =  int( sum_age / len(payload)) 
        return Response({"average": average })
    
    def find_age (self, person ):       
        td = date.today() 
        bd = person.birthday
        age_years = int((td-bd).days / 365) 
        return age_years

class Payload_filter(APIView):
    def get (self, request, from_, _to ):
        """
        filter given in range of 2 dates 2002-12-01 . 
        example of range : 
        start =  2000-12-01  
        end = 2002-12-01
        separator ":"
        request  =>   payload/filter/2000-12-01:2002-12-01
        """
        payload = Payload.objects.all()
        td = date.today()
        
        from_= list ( map( int,from_.split("-")))
        _to =  list ( map( int,_to.split("-")))
        
        #filter 
        start = td.replace(year= from_[0], month=from_[1],day=from_[2],)
        end = td.replace(year=_to[0], month=_to[1], day=_to[2])
        filtered = payload.filter(birthday__range=(start, end))
        
        serializer = PayloadSerializer(filtered, many=True)
        return Response(serializer.data)
    


class PayloadDetail(APIView):
    """
    Retrieve, update or delete a payload instance.
    """
    def get_object(self, pk):
        try:
            return Payload.objects.get(pk=pk)
        except Payload.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        payload = self.get_object(pk)
        serializer = PayloadSerializer(payload)
        return Response(serializer.data)
